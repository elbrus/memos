Introduction
============

This page describes a proposal to change the way architectures are
handled in Debian. The main idea is to make the gap between the
release architectures (`relarchs`) and the ports smaller by introducing
more flavors. The in-between flavors bring some of the feature of
`relarchs` to the ports.


Rationale
=========

The desire for this change comes from several aspects that it should
enable or make easier:

 * interested ports get suites to improve the user experience of their users

 * demoting a `relarch` to a not fully supported arch is less painful
   for the architecture as the impact is smaller, which in turn makes
   it easier for the Release Team (RT) to demote `relarchs` during the
   architecture qualification. The architecture qualification is
   currently a process that doesn't work well.

 * currently ports use `mini-dak` which is good for creating a new
   port, but less for maintaining ports.

Current state
=============

In the current state (2022), an architecture in Debian is either an
official relarch or a port. The difference between the two flavors is
rather big and going from one to the other is quite a bit of work. The
differences include:

 * testing and stable: `relarchs` have multiple suites, handled by the
   RT while ports only have `unstable`. Sometimes ports generate an
   installer image at release time.

 * `relarchs` are served via ftp.debian.org and its mirrors which are
   maintained by ftpmaster while ports are served via ports.debian.org
   [TODO, check domain], which is maintained by the Ports Team.

 * buildds for `relarchs` are maintained by DSA, while buildds for ports
   are maintained by the Ports Team.

 * the archive for `relarchs` are managed by `dak`, which is managed by
   ftpmaster, while ports use `mini-dak` managed by the Ports Team.

 * On ports, binaries may be lacking their matching source
   version (because that's how `mini-dak` works).


Open issue
==========

Who demands that an architecture needs to be rebootstrapped on DSA
maintained buildds? [elbrus: I think this is a logical requirement for
entering the ftpmaster maintained official archive]

Is it correct that multi arch can't be support on OoS?

How do the cross-compilers interact?

Is keeping source packages around longer going to be a problem? (size)

What happens with source package that only build binaries on non-FS
architectures? (e.g. do they get rejected)?


Desired state
=============

Fully supported (FS)
--------------------

 * Equal to current `relarchs`

 * The RT would like to add autopkgtest support as a requirement

 * Failing autopkgtest are RC

 * The security team considers these architectures when judging if a
   security upload is warranted.

Out of sync (OoS) / Best effort (BE)
-----------------

 * Equal to FS with the following delta's

 * binaries that are not up-to-date do not block FS, and are synced
   when ready.

 * Out of sync is tolerated for a certain time (e.g. 60 days) after
   which autoremoval will activate (key package exception).

 * `autopkgtest` failure causes autoremoval

 * `multi-arch` is not supported due to potential version skew

 * Issues on OoS archs are never of severity serious or higher
   (i.e. they will not cause migration delay or autoremoval)

 * Architecture specific problems are the responsibility of the Ports
   Team. Maintainers should accept all reasonable patches in a timely
   fashion.

 * The Ports Team can block migrations on OoS architecture (and thus
   cause more out-of-sync).

 * The Ports Team can hint removal on OoS architecture (e.g. to remove
   issue packages or to enable migration of other packages)

 * In the past, something equivalent to this flavor has been used for
   technology preview (e.g. `kfreebsd-*`) and for `relarchs` that were
   no longer supportable (before they were moved to the ports
   infrastructure), called `OUTOFSYNC_ARCHES` in
   `britney2`. `britney2` also has `BREAK_ARCHES` which is related.

 * DSA needs to be able to run buildds and porterboxes. If porters are
   not able to keep the packages that are in the installed set for a
   certain time (e.g. 30 days no activity on those bugs) the
   architecture no longer qualifies for OoS. See
   https://dsa.debian.org/ports/hardware-requirements/

 * No official security support. Security issues that also affect FS
   architectures are covered automatically. For security issues that
   don't effect FS architectures, the security team may decide do a
   security upload, but they can also defer.

Preview with suites (PwS)
-----------------------------

 * Equal to OoS with the following delta's

 * buildd's are maintained by the Ports Team

 * available from ports.debian.org

 * autopkgtest not required, but desirable

Simple preview (SP)
------------------

 * Equal to current ports

 * Equal to PwS with the following delta's

 * Only `unstable`


Alternatives
============

 * OoS, PwS and SP (or only PwS and SP, or only SP) can enable build
   profiles to drop support for features that are too expensive to
   support.

 * If ftpmasters agree PwS could be hosted on ftp.debian.org and
   mirrors, but maybe mirrors want to opt out.


Work needed
===========

Split between FS and OoS
------------------------

`britney2` needs changes to support:

 * [must] autopktest policy to not block on OoS archs
 
 * port specific hint permissions

`autoremoval` needs changes to support:

 * autoremoval of autopkgtest failures

Adding support for `testing` in PwS
-----------------------------------

`britney2` needs changes to support:

 * synchronise FS status with ports instance to prevent PwS going
   ahead of FS

`dak` needs changes to replace `mini-dak` on ports:

 * see http://pad.zammit.org/p/p/debian-ports-dak for notes

Timeline
========

Splitting


Artifacts of the proposal to consider
=====================================

 * When an arch moves from PwS to OoS, `unstable` will be
   rebootstrapped and `testing` will be filled again from that, as
   we demand binaries to be built on DSA maintained buildds.

 * When an arch moves from PwS to OoS, `autopkgtest` failures will
   block migration, so before the arch can become FS, these need to be
   resolved or accepted (e.g. because `britney2` incorrectly tries to
   test the package while its arch:all packages are not installable on
   the new FS arch).
