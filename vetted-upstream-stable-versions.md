Proposal to introduce a vetted upstream list
============================================

This document contains a proposal to introduce a list maintained by
the Release Team that contains (key) source packages with approved
upstreams stable branch policies. The idea is that the Release Team
can (automatically?) accept new upstream release during the freeze
for packages on that list.

Current state (ref: bookworm)
=============================

During the bookworm release cycle, we had several requests to approve
a new upstream version for key packages that have a sane stable branch
update policy. Several of these package are even getting a new
upstream release wildcard for security uploads by the security team or
have seen new upstream versions in past Point Releases. Reviewing and
discussing during the freeze is stressful because of the time pressure
that's present, so moving it to another time might help.

The idea
========

Instead of reviewing the upstream stable branch update policy and
discussing what the Release Team is trying to achieve during the
freeze, it might be worthwhile if we can do that before the
freeze. Additionaly we could have the maintainers of the affected
package acknowledge (e.g. by asking them to do that explicitly in a
merge request that adds their package to the list) that they
understand the ideas behind the freeze policy. We could either just
document that the review was performed, or ideally we embed the list
into the tooling. My proposal would be to work one release using the
list in a manual way and assess how it worked.

Considerations for automation
=============================

If the version in testing is from the same stable upstream branch as
the version in unstable, it won't be blocked (we could document the
vetted branch version in the list). Maybe obvious, but it is essential
for this that the packaging part of the package must be minimal
(ideally only d/changelog, but minor changes can't be ruled out). The
maintainer must refrain from other changes. A possibility is to check
if there are no changes in ./debian other than ./debian/changelog and
don't apply the wildcard if there are. Currently britney2 only works
on Sources and prefetched data, so either this check would need to be
done before, or britney2 needs to schedule it somehow. Running this
*during* britneys run doesn't seem to match the design of britney2.

Eligible
========

Packages that are acceptable by the Security Team for inclusion in a
security upload already could be automatically on the list. Packages
must be following upstream stable branches (at least during the
freeze). Their upstream must document how they manage their stable
branch, e.g. what kind of changes are eligible for their stable
branch, how changes are tested. Our FAQ has a (non exhaustive)
checklist. If the upstream policy aligns with our freeze criteria,
the package could go on the list.

Why only key packages
=====================

This proposal is only about key packages, because our current
(bookworm) freeze policy already enables maintainers to judge for
themselves until late in the freeze. So it's not a problem we need to
solve until we change the freeze policy.

References
==========

Ubuntu has a similar list, but then for stable uploads:
https://wiki.ubuntu.com/StableReleaseUpdates#Documentation_for_Special_Cases
